import io
from rest_framework import serializers
from .models import Task
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        # fields = '__all__'
        fields = ['title', 'description', 'created_at', 'due_date', 'completed']


# class TaskSerializer(serializers.Serializer):
#     title = serializers.CharField(max_length=200)
#     description = serializers.CharField(read_only=True)
#     created_at = serializers.DateTimeField(read_only=True)
#     due_date = serializers.DateField()
#     completed = serializers.BooleanField(default=False)
#
#     def create(self, validated_data):
#         instance = Task.objects.create(**validated_data)
#         return instance
#
#     def update(self, instance, validated_data):
#         instance.title = validated_data.get('title', instance.title)
#         instance.description = validated_data.get('description', instance.description)
#         instance.due_date = validated_data.get('due_date', instance.due_date)
#         instance.completed = validated_data.get('completed', instance.completed)
#         instance.save()
#         return instance











