from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Task
from .serializers import TaskSerializer
from django.forms import model_to_dict


class TaskApiList(generics.ListCreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


# class TaskApiView(APIView):
#
#     def get(self, request):
#         task_queryset = Task.objects.all()
#         return Response({'tasks': TaskSerializer(task_queryset, many=True).data})
#
#     def post(self, request):
#         serializer = TaskSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response({'task': serializer.data})
#
#     def put(self, request, *args, **kwargs):
#         pk = kwargs.get('pk')
#
#         if not pk:
#             return Response({'error': 'Method PUT not allowed'})
#
#         try:
#             instance = Task.objects.get(pk=pk)
#
#         except:
#             return Response({'error': 'Task not found'})
#
#         serializer = TaskSerializer(data=request.data, instance=instance)
#         serializer.is_valid()
#         serializer.save()
#         return Response({'task': serializer.data})
#
#     def delete(self, request, *args, **kwargs):
#         pk = kwargs.get('pk')
#
#         if not pk:
#             return Response({'error': 'Method PUT not allowed'})
#
#         try:
#             instance = Task.objects.get(pk=pk)
#             instance.delete()
#
#         except:
#             return Response({'error': 'Object not found'})
#
#         return Response({'task': f'Object {pk} was deleted'})


